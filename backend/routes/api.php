<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;




Route::post('register', [AuthController::class, 'register']); 
Route::post('login', [AuthController::class, 'login']); 
Route::middleware('auth:api')->post('logout', [AuthController::class, 'logout']);



Route::middleware('auth:api')->group(function () {
    Route::get('users', [UserController::class, 'index']); // Obtener todos los usuarios
    Route::get('users/{id}', [UserController::class, 'show']); // Obtener un usuario por su ID
    Route::post('usersNew', [UserController::class, 'store']); // Crear un nuevo usuario
    Route::put('usersEdit/{id}', [UserController::class, 'update']); // Actualizar un usuario
    Route::delete('usersDelete/{id}', [UserController::class, 'destroy']); // Eliminar un usuario
});
